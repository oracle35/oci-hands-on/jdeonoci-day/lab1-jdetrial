# lab1-jdetrial

Deploy JD Edwards Trial Edition on OCI

## Getting started

The objective of this lab is to deploy the latest JD Edwards version, Release 22 (Tools 9.2.6.3) from the OCI Market Place into your OCI tenant.
Upon completion of this tutorial, you will have a working deployment of JD Edwards EnterpriseOne Trial Edition with Tools Release 9.2 and Applications Release 9.2 with Orachstrator Studio. You can use it to verify functionality and to investigate proofs of concept (POC).

This environment will then serve as a foundation for the rest of the labs. 

## target architecture

  ![JDE Trial Architecture](./images/jdetrial-architecture.png)

## lab steps

> note: the steps described here suppose your OCI account has been provisioned with the dedicated registration application provided during the workshop. This application automatically creates your account, role, compartment and policies. If you want to follow this lab using a Free tenant you may have to create those resources manually before starting here. You can use the instructions from the dedicated LieveLab [Lift and Shift JDE to OCI Workshop](https://apexapps.oracle.com/pls/apex/dbpm/r/livelabs/view-workshop?wid=725)

### Task 1. create a VCN

Create a Virtual Cloud Network.

1. Sign in to OCI tenancy; from the Oracle CLoud Console home page, under **Launch Resources**, click **Set up a network with a wizard**.

    ![vcn-wizard](./images/vcn-wizard.png)

2. Select your compartment **jdeday-comp-username**

    ![change-compartment](./images/select-compartment.png)

3. Select **VCN with Internet Connectivity**, and then click **Start VCN Wizard**.

    ![vcn-create](./images/internet-connectivity.png)

4. In this window, fill in the following fields with the information shown below:

    * VCN NAME: ***vcn_yourname***
    * COMPARTMENT: ***jdeday-comp_yourname***
    * VCN CIDR BLOCK: 10.0.0.0/16
    * PUBLIC SUBNET CIDR BLOCK: 10.0.2.0/24
    * PRIVATE SUBNET CIDR BLOCK: 10.0.1.0/24
    * USE DNS HOSTNAMES IN THIS VCN: Make sure this is selected.

    ![vcn-create2](./images/vcn-create2.png)

  Then, scroll down to the bottom and click **Next**.

5. On the Review and Create page, click on **Create**.

6. On the Created Virtual Cloud Network page wait until you see the following graphic; then click on **View Virtual Cloud Network**

    ![vcn-created](./images/vcn-finished.png)


### Task 2. Establish Security List Rules for JDE

With the VCN in place, define the open inbound and outbound ports that will be available to instances created within the VCN.

1. From the details page of the TestDriveVCN, under the **Resources** section in the left pane, select **Security Lists**.

    ![](./images/security-lists.png " ")

2. In the **Security Lists** section, click **Default Security List for TestDriveVCN** link.

    ![](./images/default-security-list.png " ")

3. On Default Security List, under **Resources** section, click **Add Ingress Rules**.
    ![](./images/ingress-rules.png " ")

4. Set a new ingress rule with the following properties:
    1. **STATELESS**: unchecked
    2. **SOURCE TYPE**: CIDR
    3. **SOURCE CIDR**: 0.0.0.0/0
    4. **IP PROTOCOL**: TCP
    5. **SOURCE PORT RANGE**: All
    6. **DESTINATION PORT RANGE**: 443,7072,7070,7077,8079,8080,22
    7. **DEESCRIPTION**: JDE Trial

    Click **Add Ingress Rules** when complete.
    
    ![](./images/security-list-details2.png " ")

    These Ingress Rules will be sufficient to allow the network traffic required for the JDE Trial Edition.


### Task 3. Provision Trial Edition from Oracle Cloud Infrastructure Marketplace

Create an instance in OCI that is based off the JDE Trial Edition image.

1. On the Oracle Cloud Console home page, click the **Navigation** Menu Button in the upper-left corner and hover over **Marketplace** and select **All Applications**.

    ![](./images/marketplace.png " ")

2.  Locate the Oracle JD Edwards image tile for **JD Edwards EnterpriseOne Trial Edition** (you might have to search for it; there could be several images out there) and click the tile.

    ![](./images/jde-trial-edition-image.png " ")

3.  On the information page for the JD Edwards EnterpriseOne Trial Edition image, select the version **(9.2.6.3 – default)** to deploy and the compartment (you created a compartment in Set Up Oracle Cloud Infrastructure for JDE Trial Edition Deployment, Step 2) to deploy to. Select the check box to accept the Oracle Standard Terms and Restrictions and then click **Launch Instance**.

    ![](./images/launch-jdetrial.png " ")

4.  Next, define the instance with the following options:

    1. Instance Name:

        ```
        jdetrial
        ```

       ![](./images/instance-details.png " ")

    2. Depending on the region selected, there might be one or more availability domains. Select Availability Domain: **AD1** (if single AD available) or **AD3** (if multiple ADs available).

    3. Operating System or Image Source: Leave **JD Edwards EnterpriseOne Trial Edition** selected.

    4. Instance Shape: keep **VM.Standard.E4.Flex** shape with 1 OCPU & 16 Gb RAM

    5. Review and confirm the Virtual Cloud Network and Subnets you created earlier are selected. Ensure that the **Assign a public IP address** radio button is selected.

       ![](./images/instance-details-network.png " ")

    6. In the Add SSH keys section, select the **Generate a Key pair for me** radio button. Click **Save private key** and **Save public key** to download both files on your laptop. They'll be use to connect to the VM and finalize the configuration.

       ![](./images/ssh-keys.png " ")

    You should have 2 files on your laptop:
      * private key: **ssh-key-2022-09-28.key**
      * pubic key: **ssh-key-2022-09-28.key.pub**


    7. Leave the Boot Volume section as is.

       ![](./images/boot-size.png " ")


    8. Click **Create**.

       ![](./images/create-button.png " ")

    You've now finished the provisioning step and should now see the screen below. The orange box indicates that the instance is in the process of being provisioned.

    ![](./images/orange-instance.png " ")

    **Note:** After a few minutes, the instance will be running and ready.

5.  Copy and take note of the **Public IP address** under the **Instance Access** section, which is required to connect to the instance.

    ![](./images/finished-instance.png " ")

## Task 3:  Access the Oracle Cloud Infrastructure Instance

To complete the setup of the JD Edwards EnterpriseOne Trial Edition, it is necessary to connect to the VM Instance.

The user name for the instance is **opc**  and there isn’t a password. The instance can only be accessed using the SSH private key.

OCI Console provides an integrated **Cloud Shell** to faciliate this connection.

1. on the top right side of the OCI console, click on the **Cloud Shell** icon

![](./images/cloud-shell.png " ")

After a few seconds, you'll have a fully functional shell at the bottom of your navigator window:

![](./images/cloud-shell-2.png " ")

2. Upload the private Key File to the Cloud Shell: click on the **gear button** on the top right and select **upload**

![](./images/cloud-shell-upload.png " ")

3. Select the private key **ssh-key-2022-09-28.key** you downloaded previously and click **upload**

![](./images/cloud-shell-upload-key.png " ")

4. Update the file read-write access:

```bash
chmod 400 ssh-key-2022-09-28.key
```

5. Create a SSH connection to the JDE Trial instance

```bash
ssh –i ssh-key-2022-09-28.key opc@<JDE Intance Public IP>
````

6. During the first connection to a Trial Edition instance, a process will run to verify if any critical updates are required to the OS and the installed software packages on the instance. If so, the following screen will display noting that the yum update packages are installing. This process can take anywhere from 5 to 10 minutes to run.

![](./images/jdetrial-update.png " ")

Once complete, the VM is automatically restarted and you should be disconnected.

7. Connect again to the JDE Trial instance (same step as 5.);  follow along and answer the following prompts to complete the configuration:

```bash
- HTML Port: 8080
- DB SYS Password: Jde_Rules1#                         <== 8 to 10 char, +1 letter, +1 number, no $,|, @ 
- JDE User Password: Jde_Rules1
- Weblogic Admin Password: Jde_Rules1
- Confirm: Y
```

> Notes: 
>   **HTML Port** must match the port number added to the ingress rules for the security list.

>   The **DB SYS password** must meet these guidelines if you create your own:

        * Must be between 8 and 10 characters.
        * Must contain at least 1 letter and 1 number.
        * May not contain any shell metadata characters such as $, |, @, and so on.

>   For simplicity, use the same password for **JDE User Password** and **Weblogic Admin Password**

Configuration will take between **25 to 30 minutes** - Time for a break! 

The configuration will go through and change all necessary database records and files on the system for the system information and options entered, as well as start all necessary services. Once complete, the JD Edwards EnterpriseOne Trial Edition is ready for use. Watch for the status “Successfully completed u01/vmScripts/EOne_Sync.sh”.

![jdetrial-success.png](./images/jdetrial-success.png)

You can use the URL to connect to JD Edwards and Orchestrator:

![jdetrial-urls.png](./images/jdetrial-urls.png)

👋 You successfully deployed the JDE Trial instance. You can now either start discovering the latest features, especially with Orchestrator or continue your OCI workshop with the second lab: [lab2-jdetrial-with-elb](https://gitlab.com/oracle35/oci-hands-on/jdeonoci-day/lab2-jdetrial-with-elb)
